package ictgradschool.industry.lab17.ex04;

import com.sun.media.sound.InvalidFormatException;
import ictgradschool.Keyboard;
import ictgradschool.industry.lab17.ex02.InvalidDataFormatException;

import java.util.IllegalFormatConversionException;
import java.util.IllegalFormatException;

/**
 * Created by qpen546 on 15/05/2017.
 */
public class AreaCalculator {
    public double width;
    public double length;
    public double radius;

    public void start() {
        System.out.println("Welcome to Shape Area Calculator\n");
        while (true) {
            try {
                System.out.print("Enter the width of the rectangle: ");
                width = convertToDouble(Keyboard.readInput());
                System.out.print("Enter the length of the rectangle: ");
                length = convertToDouble(Keyboard.readInput());
                break;
            } catch (InvalidFormatException e) {
                System.out.println("input is invalid number");
            } catch (NumberFormatException e) {
                System.out.println("input is not a number");
            }
        }

        int rectangleArea = getRoundNumber(getRectangleArea(width, length));

        radius = 20;
        System.out.println("\nThe radius of the circle is: " + radius);

        int circleArea = getRoundNumber(getCircleArea(radius));
        int result = Math.min(rectangleArea, circleArea);
        System.out.print("The smaller area is: " + result);

    }

    public double convertToDouble(String string) throws NumberFormatException, InvalidFormatException {
        Double input;
        input = Double.parseDouble(string);
        if (input <= 0) {
            throw new InvalidFormatException();
        }

        return input;
    }

    public double getRectangleArea(double width, double height) {
        return width * height;
    }

    public double getCircleArea(double radius) {
        return Math.PI * Math.pow(radius, 2);
    }

    public int getRoundNumber(double numbers) {
        return (int) Math.round(numbers);
    }

    public static void main(String[] args) {
        new AreaCalculator().start();
    }
}
