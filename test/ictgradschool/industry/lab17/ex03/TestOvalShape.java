package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by qpen546 on 15/05/2017.
 */
public class TestOvalShape {
    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    /** Tests whether the {@link RectangleShape}'s default constructor functions as expected. */
    @Test
    public void testDefaultConstructor() {
        OvalShape shape = new OvalShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        // Check that the paint method caused a rectangle at position (0, 0), with size (25, 35), to be drawn.
        shape.paint(painter);
        assertEquals("(oval 0,0,25,35)", painter.toString());
    }

    /** Tests whether the {@link RectangleShape}'s constructor which takes speed arguments functions as expected. */
    @Test
    public void testConstructorWithSpeedValues() {
        OvalShape shape = new OvalShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(oval 1,2,25,35)", painter.toString());
    }

    /** Tests whether the {@link RectangleShape}'s constructor which takes all arguments functions as expected. */
    @Test
    public void testConstructorWithAllValues() {
        OvalShape shape = new OvalShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(oval 1,2,5,6)", painter.toString());
    }

    /** Tests whether just moving a {@link RectangleShape} works, with no bouncing involved. */
    @Test
    public void testSimpleMove() {
        OvalShape shape = new OvalShape(100, 20, 12, 15);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        // Checks that two rectangles were drawn (one for each call to "paint").
        // The first one should be at the initial position (100, 20), and the second one should be at
        // position (112, 35) (i.e. x + deltaX, y + deltaY). The width and height should be (25, 35) in both cases.
        assertEquals("(oval 100,20,25,35)(oval 112,35,25,35)",
                painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the right wall. */
    @Test
    public void testShapeMoveWithBounceOffRight() {
        OvalShape shape = new OvalShape(100, 20, 12, 15);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);

        // Checks that three rectangles were drawn (one for each call to "paint").
        // The first one should be at the initial position (100, 20). The second one should be at position (110, 35)
        // because it hit the right wall (110 is the largest possible x value since the width of the space the shape
        // is moving in is 135, and 135 (the area's width) - 25 (the shape's width) = 110).
        // The third rectangle should be at position (98, 50) because the deltaX should have reversed as the shape bounced,
        // and it should have moved 12 pixels to the left.
        assertEquals("(oval 100,20,25,35)(oval 110,35,25,35)"
                + "(oval 98,50,25,35)", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the left wall. */
    @Test
    public void testShapeMoveWithBounceOffLeft() {
        OvalShape shape = new OvalShape(10, 20, -12, 15);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(oval 10,20,25,35)(oval 0,35,25,35)"
                + "(oval 12,50,25,35)", painter.toString());
    }


    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the top wall. */
    @Test
    public void testShapeMoveWithBounceOffTop() {
        OvalShape shape = new OvalShape(10, 10, 0, -15);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(oval 10,10,25,35)(oval 10,0,25,35)(oval 10,15,25,35)", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the bottom wall. */
    @Test
    public void testShapeMoveWithBounceOffBottom() {
        OvalShape shape = new OvalShape(10, 20, 0, 15);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        assertEquals("(oval 10,20,25,35)(oval 10,25,25,35)(oval 10,10,25,35)", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the bottom-left corner. */
    @Test
    public void testShapeMoveWithBounceOffBottomAndLeft() {
        OvalShape shape = new OvalShape(10, 90, -12, 15);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        assertEquals("(oval 10,90,25,35)(oval 0,100,25,35)"
                + "(oval 12,85,25,35)", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the bottom-right corner. */
    @Test
    public void testShapeMoveWithBounceOffBottomAndRight() {
        OvalShape shape = new OvalShape(70, 60, 10, 10);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        assertEquals("(oval 70,60,25,35)(oval 75,65,25,35)(oval 65,55,25,35)", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the top-right corner. */
    @Test
    public void testShapeMoveWithBounceOffTopAndRight() {
        OvalShape shape = new OvalShape(70, 5, 10, -10);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        assertEquals("(oval 70,5,25,35)(oval 75,0,25,35)(oval 65,10,25,35)", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the top-left corner. */
    @Test
    public void testShapeMoveWithBounceOffTopAndLeft() {
        OvalShape shape = new OvalShape(5, 5, -10, -10);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        assertEquals("(oval 5,5,25,35)(oval 0,0,25,35)(oval 10,10,25,35)", painter.toString());
    }
}
