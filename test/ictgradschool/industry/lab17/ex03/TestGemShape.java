package ictgradschool.industry.lab17.ex03;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by qpen546 on 15/05/2017.
 */
public class TestGemShape {
    private MockPainter painter;

    @Before
    public void setUp() {
        painter = new MockPainter();
    }

    /** Tests whether the {@link RectangleShape}'s default constructor functions as expected. */
    @Test
    public void testDefaultConstructor() {
        GemShape shape = new GemShape();

        assertEquals(0, shape.getX());
        assertEquals(0, shape.getY());
        assertEquals(5, shape.getDeltaX());
        assertEquals(5, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        // Check that the paint method caused a rectangle at position (0, 0), with size (25, 35), to be drawn.
        shape.paint(painter);
        assertEquals("(polygon xpoints: [0, 12, 25, 12], ypoints: [17, 0, 17, 35])", painter.toString());
    }

    /** Tests whether the {@link RectangleShape}'s constructor which takes speed arguments functions as expected. */
    @Test
    public void testConstructorWithSpeedValues() {
        GemShape shape = new GemShape(1, 2, 3, 4);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(25, shape.getWidth());
        assertEquals(35, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 13, 26, 13], ypoints: [19, 2, 19, 37])", painter.toString());
    }

    /** Tests whether the {@link RectangleShape}'s constructor which takes all arguments functions as expected. */
    @Test
    public void testConstructorWithAllValues() {
        GemShape shape = new GemShape(1, 2, 3, 4, 5, 6);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(5, shape.getWidth());
        assertEquals(6, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 3, 6, 3], ypoints: [5, 2, 5, 8])", painter.toString());
    }
    @Test
    public void testConstructorWithAllValues2() {
        GemShape shape = new GemShape(1, 2, 3, 4, 60, 60);

        assertEquals(1, shape.getX());
        assertEquals(2, shape.getY());
        assertEquals(3, shape.getDeltaX());
        assertEquals(4, shape.getDeltaY());
        assertEquals(60, shape.getWidth());
        assertEquals(60, shape.getHeight());

        shape.paint(painter);
        assertEquals("(polygon xpoints: [1, 21, 41, 61, 41, 21, 0, 0], ypoints: [32, 2, 2, 32, 62, 62, 0, 0])", painter.toString());
    }

    /** Tests whether just moving a {@link RectangleShape} works, with no bouncing involved. */
    @Test
    public void testSimpleMove() {
        GemShape shape = new GemShape(100, 20, 12, 15);
        shape.paint(painter);
        shape.move(500, 500);
        shape.paint(painter);

        // Checks that two rectangles were drawn (one for each call to "paint").
        // The first one should be at the initial position (100, 20), and the second one should be at
        // position (112, 35) (i.e. x + deltaX, y + deltaY). The width and height should be (25, 35) in both cases.
        assertEquals("(polygon xpoints: [100, 112, 125, 112], ypoints: [37, 20, 37, 55])(polygon xpoints: [112, 124, 137, 124], ypoints: [52, 35, 52, 70])",
                painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the right wall. */
    @Test
    public void testShapeMoveWithBounceOffRight() {
        GemShape shape = new GemShape(100, 20, 12, 15);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);
        shape.move(135, 10000);
        shape.paint(painter);

        // Checks that three rectangles were drawn (one for each call to "paint").
        // The first one should be at the initial position (100, 20). The second one should be at position (110, 35)
        // because it hit the right wall (110 is the largest possible x value since the width of the space the shape
        // is moving in is 135, and 135 (the area's width) - 25 (the shape's width) = 110).
        // The third rectangle should be at position (98, 50) because the deltaX should have reversed as the shape bounced,
        // and it should have moved 12 pixels to the left.
        assertEquals("(polygon xpoints: [100, 112, 125, 112], ypoints: [37, 20, 37, 55])(polygon xpoints: [110, 122, 135, 122], ypoints: [52, 35, 52, 70])(polygon xpoints: [98, 110, 123, 110], ypoints: [67, 50, 67, 85])", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the left wall. */
    @Test
    public void testShapeMoveWithBounceOffLeft() {
        GemShape shape = new GemShape(10, 20, -12, 15);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 22, 35, 22], ypoints: [37, 20, 37, 55])(polygon xpoints: [0, 12, 25, 12], ypoints: [52, 35, 52, 70])(polygon xpoints: [12, 24, 37, 24], ypoints: [67, 50, 67, 85])", painter.toString());
    }


    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the top wall. */
    @Test
    public void testShapeMoveWithBounceOffTop() {
        GemShape shape = new GemShape(10, 10, 0, -15);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        shape.move(10000, 10000);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 22, 35, 22], ypoints: [27, 10, 27, 45])(polygon xpoints: [10, 22, 35, 22], ypoints: [17, 0, 17, 35])(polygon xpoints: [10, 22, 35, 22], ypoints: [32, 15, 32, 50])", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the bottom wall. */
    @Test
    public void testShapeMoveWithBounceOffBottom() {
        GemShape shape = new GemShape(10, 20, 0, 15);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        shape.move(10000, 60);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 22, 35, 22], ypoints: [37, 20, 37, 55])(polygon xpoints: [10, 22, 35, 22], ypoints: [42, 25, 42, 60])(polygon xpoints: [10, 22, 35, 22], ypoints: [27, 10, 27, 45])", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the bottom-left corner. */
    @Test
    public void testShapeMoveWithBounceOffBottomAndLeft() {
        GemShape shape = new GemShape(10, 90, -12, 15);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        shape.move(125, 135);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [10, 22, 35, 22], ypoints: [107, 90, 107, 125])(polygon xpoints: [0, 12, 25, 12], ypoints: [117, 100, 117, 135])(polygon xpoints: [12, 24, 37, 24], ypoints: [102, 85, 102, 120])", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the bottom-right corner. */
    @Test
    public void testShapeMoveWithBounceOffBottomAndRight() {
        GemShape shape = new GemShape(70, 60, 10, 10);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [70, 82, 95, 82], ypoints: [77, 60, 77, 95])(polygon xpoints: [75, 87, 100, 87], ypoints: [82, 65, 82, 100])(polygon xpoints: [65, 77, 90, 77], ypoints: [72, 55, 72, 90])", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the top-right corner. */
    @Test
    public void testShapeMoveWithBounceOffTopAndRight() {
        GemShape shape = new GemShape(70, 5, 10, -10);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [70, 82, 95, 82], ypoints: [22, 5, 22, 40])(polygon xpoints: [75, 87, 100, 87], ypoints: [17, 0, 17, 35])(polygon xpoints: [65, 77, 90, 77], ypoints: [27, 10, 27, 45])", painter.toString());
    }

    /** Tests whether moving a {@link RectangleShape} works, when it bounces off the top-left corner. */
    @Test
    public void testShapeMoveWithBounceOffTopAndLeft() {
        GemShape shape = new GemShape(5, 5, -10, -10);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        shape.move(100, 100);
        shape.paint(painter);
        assertEquals("(polygon xpoints: [5, 17, 30, 17], ypoints: [22, 5, 22, 40])(polygon xpoints: [0, 12, 25, 12], ypoints: [17, 0, 17, 35])(polygon xpoints: [10, 22, 35, 22], ypoints: [27, 10, 27, 45])", painter.toString());
    }
}
