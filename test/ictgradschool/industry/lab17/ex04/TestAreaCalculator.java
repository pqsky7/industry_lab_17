package ictgradschool.industry.lab17.ex04;

import com.sun.media.sound.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by qpen546 on 15/05/2017.
 */
public class TestAreaCalculator {
    private AreaCalculator areaCalculator;

    @Before
    public void setUp() {
        areaCalculator = new AreaCalculator();
    }


    @Test
    public void testConvertToDouble() {
        double actual = 0;
        try {
            actual = areaCalculator.convertToDouble("5");
        } catch (InvalidFormatException e) {
            fail();
        }
        assertEquals(5.0, actual, 0.01);

        try {
            actual = areaCalculator.convertToDouble("-5");
            fail();
        } catch (InvalidFormatException e) {
            assertEquals(true,true);
        }

        try {
            actual = areaCalculator.convertToDouble("a");
        } catch (NumberFormatException e) {
            assertEquals(true,true);
        } catch (InvalidFormatException e) {
            fail();
        }
    }

    @Test
    public void testGetRectangleArea() {
        double actual = areaCalculator.getRectangleArea(5,5);
        assertEquals(25, actual, 0.01);
    }

    @Test
    public void testGetCircleArea() {
        double actual = areaCalculator.getCircleArea(5);
        assertEquals(78.53, actual, 0.01);
    }

    @Test
    public void testRoundNumber() {
        double actual = areaCalculator.getRoundNumber(5.5);
        assertEquals(6, actual, 0.01);
    }

}
