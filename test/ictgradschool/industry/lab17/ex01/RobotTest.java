package ictgradschool.industry.lab17.ex01;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class RobotTest {

    private Robot myRobot;

    @Before
    public void setUp() {
        myRobot = new Robot();
    }

    @Test
    public void testRobotConstruction() {
        assertEquals(Robot.Direction.North, myRobot.getDirection());
        assertEquals(10, myRobot.row());
        assertEquals(1, myRobot.column());
    }

    @Test
    public void testIllegalMoveNorth() {
        boolean atTop = false;
        try {
            // Move the robot to the top row.
            for (int i = 0; i < Robot.GRID_SIZE - 1; i++)
                myRobot.move();

            // Check that robot has reached the top.
            atTop = myRobot.currentState().row == 1;
            assertTrue(atTop);
        } catch (IllegalMoveException e) {
            fail();
        }

        try {
            // Now try to continue to move North.
            myRobot.move();
            fail();
        } catch (IllegalMoveException e) {
            // Ensure the move didn’t change the robot state

            assertEquals(1, myRobot.currentState().row);
        }
    }

    private Robot.Direction[] Directions = {Robot.Direction.North, Robot.Direction.East, Robot.Direction.South, Robot.Direction.West};
    private int[][] position = {{1, 1}, {10, 10}, {10, 10}, {1, 1}};
    @Test
    public void turn() {

        Robot.Direction[] results = {Robot.Direction.East, Robot.Direction.South, Robot.Direction.West, Robot.Direction.North};
        for (int i = 0; i < 4; i++) {
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], 7, 7);
            myRobot.states.add(robotState);
            myRobot.turn();
            assertEquals(results[i], myRobot.getDirection());
            assertEquals(myRobot.row(), 7);
            assertEquals(myRobot.column(), 7);
        }
    }

    @Test
    public void move() throws IllegalMoveException {

        int[][] results = {{6, 7}, {7, 8}, {8, 7}, {7, 6}};
        for (int i = 0; i < 4; i++) {
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], 7, 7);
            myRobot.states.add(robotState);
            try {
                myRobot.move();
                assertEquals(results[i][0], myRobot.row());
                assertEquals(results[i][1], myRobot.column());
            } catch (IllegalMoveException e) {
                fail();
            }

            Robot.RobotState robotState2 = new Robot().new RobotState(Directions[i], position[i][0], position[i][1]);
            myRobot.states.add(robotState2);
            try {
                myRobot.move();
                fail();
            } catch (IllegalMoveException e) {
                assertEquals(position[i][0],myRobot.row());
                assertEquals(position[i][1],myRobot.column());
            }

        }
    }

    @Test
    public void testGetDirection(){
        for (int i = 0; i < 4; i++) {
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], 7, 7);
            myRobot.states.add(robotState);
            assertEquals(Directions[i],myRobot.getDirection());
        }
    }

    @Test
    public void testBackTrack(){
        for (int i = 0; i < 4; i++) {
            int length = myRobot.states.size();
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], position[i][0], position[i][1]);
            myRobot.states.add(robotState);
            myRobot.backTrack();
            assertEquals(length, myRobot.states.size());
        }
    }

    @Test
    public void testRow(){
        for (int i = 0; i < 4; i++) {
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], position[i][0], position[i][1]);
            myRobot.states.add(robotState);
            assertEquals(position[i][0],myRobot.row());
        }
    }

    @Test
    public void testColumn(){
        for (int i = 0; i < 4; i++) {
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], position[i][0], position[i][1]);
            myRobot.states.add(robotState);
            assertEquals(position[i][1],myRobot.column());
        }
    }

    @Test
    public void testCurrentState(){
        for (int i = 0; i < 4; i++) {
            Robot.RobotState robotState = new Robot().new RobotState(Directions[i], position[i][0], position[i][1]);
            myRobot.states.add(robotState);
            assertEquals(robotState,myRobot.currentState());
        }
    }
}
