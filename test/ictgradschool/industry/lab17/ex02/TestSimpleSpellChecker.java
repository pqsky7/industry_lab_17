package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Array;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestSimpleSpellChecker {

    @Test
    public void testFoundationsOfMathematics() {
        assertEquals(2, 1 + 1);
    }

    Dictionary dictionary = new Dictionary();
    SimpleSpellChecker checkerTest;

    @Before
    public void setUp() throws InvalidDataFormatException {
        checkerTest = new SimpleSpellChecker(dictionary, "hello hello Hello world, u r xxx 100th 10");
    }

    @Test
    public void testGetMisspelledWords() {
        List<String> results = new ArrayList<>(Arrays.asList("u", "r", "xxx"));
        Collections.sort(results);
        List<String> misspelledWords = checkerTest.getMisspelledWords();
        Collections.sort(misspelledWords);
        assertEquals(results,misspelledWords);
    }

    @Test
    public void testGetUniqueWords() {
        List<String> results = new ArrayList<>(Arrays.asList("hello", "world", "u", "r", "xxx","10","100th"));
        Collections.sort(results);
        List<String> uniqueWords = checkerTest.getUniqueWords();
        Collections.sort(uniqueWords);
        assertEquals(results,uniqueWords);
    }

    @Test
    public void testGetFrequencyOfWord() throws InvalidDataFormatException {
        int[] result = {0,1,3};
        String[] testInput = {"test","world","hello"};
        for(int i=0;i<testInput.length;i++){
            int frequency = checkerTest.getFrequencyOfWord(   testInput[i]);
            assertEquals(testInput[i]+" frequency "+result[i],testInput[i]+" frequency "+frequency);
        }
    }
}