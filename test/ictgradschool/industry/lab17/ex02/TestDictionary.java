package ictgradschool.industry.lab17.ex02;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * TODO Implement this class.
 */
public class TestDictionary {

    @Test
    public void testTrueIsTrue() {
        assertEquals(true, true);
    }


    private Dictionary dictionaryTest;

    @Before
    public void setUp(){
        dictionaryTest = new Dictionary();
    }

    @Test
    public void testConstructor(){

    }

    @Test
    public void testIsSpellingCorrect(){
        String[] testString = {"Hello","hello","HELLO","QIAN","Qian","qian","100","100ABC","ABC100","A100BC","*","*ABC","ABC*","A*BC"};
        boolean[] results = {false,true,false,false,false,false,false,false,false,false,false,false,false,false};
        for(int i=0; i<testString.length;i++){
            assertEquals(results[i], dictionaryTest.isSpellingCorrect(testString[i]));
        }
    }
}